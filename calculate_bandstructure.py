'''
Author: Tom Rodemund
Date: 08.09.2020

calculates the band structure of a cell and plots it
'''

import sys
sys.path.append('modules/')

import json

import plotting
import bandstructure as bs

def main():
    print('CALCULATING BANDSTRUCTURE')

    input_file = open('input.json')
    parameters = json.load(input_file)
    input_file.close()

    inputdir = parameters['inputdir']
    outputdir = parameters['outputdir']

    print('\ncalculating band structure...')
    
    bandstructure = bs.calculate_bandstructure(cellpath=inputdir, savedir=outputdir)

    print('calculating Fermi-energy...')

    fermienergy = bs.calculate_fermienergy(bandstructure, savedir=outputdir)

    print('\tE_F = {0:.6f} eV'.format(fermienergy))

    print('plotting...')

    plotting.bandstructure(bandstructure, savedir=outputdir, fermienergy=fermienergy)

    print('\nDONE!')

if __name__ == '__main__':
    main()