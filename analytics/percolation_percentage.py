'''
percolation_percentage_and_calculation_time.py

Author: Tom Rodemund
Date: 04.08.2020

This program creates a graphic with the percolation percentage and calculation 
time depending on various parameters. This programm is to be executed on the
cluster as it takes a lot of time.
'''

import sys
sys.path.append('../')
sys.path.append('../modules')

import numpy as np
import random
import time

import main as main_module

def main():
    # parameters
    
    length = 50
    width = 20
    n_lines_max = 100
    n_lines_list = range(1, n_lines_max + 1)
    n_linelength = 15
    layer_height = 1.42
    seed = 1
    
    n_samples = 200
    
    n_percolation_percentage_list = []
    time_needed_list = []
    
    random.seed(seed)
    
    for n_lines in n_lines_list:
        print('n_lines = {0}'.format(n_lines), end='\t')
        
        n_percolating = 0
        
        time_needed = .0
        
        for i in range(n_samples):
            time_start = time.time()
            
            nw = main_module.Network(inputdir='input/')
            
            nw.create(length, width, n_lines, n_linelength, layer_height)
            nw.remove_overhang()
            nw.initialize()
            nw.find_clusters()
            is_percolating = nw.only_percolating()
            
            time_end = time.time()
            time_needed_now = time_end - time_start
            time_needed += time_needed_now
            
            if is_percolating:
                n_percolating += 1
        
        n_percolation_percentage = n_percolating * 100. / n_samples # crepresentation in percent
        print('percolation % = {0:.2f} %'.format(n_percolation_percentage), end='\t')
        n_percolation_percentage_list.append(n_percolation_percentage)
        
        time_needed /= n_samples
        print('time/calc = {0:.3f} s\t({1:.3f} s total)'.format(time_needed, time_needed * n_samples))
        time_needed_list.append(time_needed)
    
    # saving data

    print('saving data...')

    np.savetxt('result_percolation_percentage.dat', [n_lines_list, n_percolation_percentage_list])
    np.savetxt('result_calculation_time.dat', [n_lines_list, time_needed_list])
    
    f = open('used_params.txt', 'w')
    f.write('length:\t\t{0}\n'.format(length))
    f.write('width:\t\t{0}\n'.format(width))
    f.write('n_lines_max:\t{0}\n'.format(n_lines_max))
    f.write('n_linelength:\t{0}\n'.format(n_linelength))
    f.write('layer_height:\t{0}\n'.format(layer_height))
    f.write('seed:\t\t{0}\n'.format(seed))
    f.write('n_samples:\t{0}'.format(n_samples))
    f.close()
    

if __name__ == '__main__':
    main()