import numpy as np

import matplotlib.pyplot as plt

import sys
sys.path.append('..')
import modules.plotting


# bandstructure data

bandstructure = np.loadtxt('../unit_cell/bandstructure.dat')

k_list = bandstructure[0]

# transmission data

fermienergy = np.loadtxt('../unit_cell/fermienergy.dat')

transmission = np.loadtxt('../results/transmission.dat')
transmission[0] = transmission[0]

# begin plotting

fig, ax = plt.subplots(ncols=2, sharey=True)

# plotting

for axes in ax:
    axes.axhline(.0, color='red', linestyle='dashed')

for line in bandstructure[1:]:
    ax[0].plot(k_list / np.pi, line - fermienergy, color='black')

ax[1].plot(transmission[1], transmission[0], color='black')

# cosmetics

ax[0].set_xticks([-1, 0, 1])
ax[0].set_xlim(-1, 1.)
ax[0].set_ylim(transmission[0,0], transmission[0,-1])
ax[0].set_xlabel(r'$k$ [$\pi / a$]')
ax[0].set_ylabel(r'$E - E_\mathrm{F}$ [eV]')

ax[1].set_xlabel(r'$T$')

for axes in ax:
    axes.tick_params(
        axis='both',
        direction='in'
    )

# saving

plt.tight_layout(pad=.1)
fig.savefig('transmission_bandstructure.png', dpi=300, bbox_inches='tight')