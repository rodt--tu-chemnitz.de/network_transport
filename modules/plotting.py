'''
Author: Tom Rodemund
Date: 01.09.2020

This module contains functions to plot e.g. transmission spectra.
'''

import numpy as np

import matplotlib
matplotlib.rcParams['savefig.dpi'] = 300
matplotlib.rcParams['savefig.bbox'] = 'tight'
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['xtick.top'] = True
matplotlib.rcParams['ytick.direction'] = 'in'
matplotlib.rcParams['ytick.right'] = True
matplotlib.rcParams['axes.axisbelow'] = False

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines

import modules.constants as constants


def main():
    transmission_data = np.loadtxt('../results/transmission.dat')
    transmission(transmission_data, savedir='../results/')

    bandstructure_data = np.loadtxt('../results/bandstructure.dat')
    fermienergy = np.loadtxt('../results/fermienergy.dat')
    bandstructure(bandstructure_data, savedir='../results/', fermienergy=fermienergy)


def transmission(data, savedir='results/'):
    '''
    Plots the transmission spectrum. By default reads and writes from and to the
    results folder.
    '''
    fig, ax = plt.subplots()

    ax.semilogy(*data)

    ax.set_xlabel(r'$E - E_\mathrm{F}$ [eV]')
    ax.set_ylabel(r'transmission')

    ax.set_xlim(data[0, 0], data[0, -1])

    ax.tick_params(
        axis='both',
        direction='in'
        )

    fig.savefig(savedir + 'transmission.png', bbox_inches='tight', dpi=300)
    plt.close()


def bandstructure(bandstructure, savedir='results/', fermienergy=None):
    '''
    Plots the bandstructure. By default reads and writes from and to the
    results folder.
    '''
    fig, ax = plt.subplots()

    if fermienergy is not None:
        ax.axhline(fermienergy, color='red', linestyle='--')

    # scaling x to be in [- pi/a, pi/a]
    bandstructure[0] = bandstructure[0] / np.pi

    for line in bandstructure[1:]:
        ax.plot(bandstructure[0], line, color='black')

    ax.set_xlim(bandstructure[0, 0], bandstructure[0, -1])

    ax.set_xlabel(r'$k$ [$\pi / a$]')
    ax.set_ylabel(r'$E$ [eV]')

    ax.set_xticks([-1, 0, 1])

    ax.tick_params(
        axis='both',
        direction='in'
        )

    fig.savefig(savedir + 'bandstructure_all.png', bbox_inches='tight', dpi=300)

    if fermienergy is not None:
        dx = 3. # eV

        ax.set_ylim(fermienergy - dx, fermienergy + dx)

        fig.savefig(savedir + 'bandstructure_zoom.png', bbox_inches='tight', dpi=300)
    
    plt.close()



def unit_cell(celldir='', savedir=''):
    # read data

    cellpath = celldir + 'unit_cell.xyz'

    f = open(cellpath, 'r')
    n_atoms = int(f.readline())
    vector = np.array([float(s) for s in f.readline().split('\t')])
    f.close()

    coords = np.genfromtxt(cellpath, skip_header=2, usecols=(1,2,3,))
    atomtypes = np.genfromtxt(cellpath, usecols=(0,), dtype=str, skip_header=2)

    if n_atoms == 1:
        coords = [coords]
        atomtypes = [atomtypes]

    # initialize

    fig, ax = plt.subplots()

    patches = []
    lines = []

    # size

    c_radius = constants.A_CC * .3
    h_radius = constants.A_CC * .1

    linewidth_big = 4.
    linewidth_small = linewidth_big * .5

    # colors
    color_c_main = 'black'
    color_c_neighbors = 'gray'

    color_h_main = 'mediumblue'
    color_h_neighbors = 'cornflowerblue'

    color_error = 'red'

    edgecolor = 'black'

    # circles

    for c, t in zip(coords, atomtypes):
        if t == 'C':
            radius = c_radius
            color = color_c_main
        elif t == 'H':
            radius = h_radius
            color = color_h_main
        else:
            print('WARNING: ATOM TYPE {0} NOT RECOGNIZED'.format(t))
            radius = c_radius
            color = color_error

        # main cell
        patch = mpatches.Circle(
            c[:2],
            radius=radius,
            facecolor=color,
            edgecolor=edgecolor
            )
        
        patches.append(patch)

        # neighbors

        for c_n in [c - vector, c + vector]:
            if t == 'C':
                color = color_c_neighbors
            elif t == 'H':
                color = color_h_neighbors


            patch = mpatches.Circle(
                c_n[:2],
                radius=radius,
                facecolor=color,
                edgecolor=edgecolor
                )
            
            patches.append(patch)

    for p in patches:
        ax.add_patch(p)

    # lines

    # NN cutoff to display, even though in
    # reality 3NN (2 * A_CC) is chosen
    CUTOFF = 1.1 * constants.A_CC

    # connections in cell
    for i in range(n_atoms-1):
        c_1 = coords[i]
        for j in range(i+1, n_atoms):
            c_2 = coords[j]

            dist = np.linalg.norm(c_1 - c_2)

            if dist < CUTOFF:
                linecoords_x = [c_1[0], c_2[0]]
                linecoords_y = [c_1[1], c_2[1]]

                # main cell

                if atomtypes[i] == 'H' or atomtypes[j] == 'H':
                    linewidth = linewidth_small
                else:
                    linewidth = linewidth_big

                line = mlines.Line2D(
                    linecoords_x,
                    linecoords_y,
                    color=color_c_main,
                    zorder=0,
                    linewidth=linewidth
                    )
                
                lines.append(line)

                # neighbors
                for dr in [-vector, vector]:
                    line = mlines.Line2D(
                        linecoords_x + dr[0],
                        linecoords_y + dr[1],
                        color=color_c_neighbors,
                        zorder=0,
                        linewidth=linewidth
                        )
                    
                    lines.append(line)


    # connections to next cell

    for c_1, t_1 in zip(coords, atomtypes):
        for c_2, t_2 in zip(coords + vector, atomtypes):
            dist = np.linalg.norm(c_1 - c_2)

            if dist < CUTOFF:
                linecoords_x = [c_1[0], c_2[0]]
                linecoords_y = [c_1[1], c_2[1]]

                if t_1 == 'H' or t_2 == 'H':
                    linewidth = linewidth_small
                else:
                    linewidth = linewidth_big

                # connection to next neighbor

                # to right

                line = mlines.Line2D(
                    linecoords_x,
                    linecoords_y,
                    color=color_c_neighbors,
                    zorder=0,
                    linewidth=linewidth_big
                    )
                
                lines.append(line)

                # to left

                line = mlines.Line2D(
                    linecoords_x - vector[0],
                    linecoords_y - vector[1],
                    color=color_c_neighbors,
                    zorder=0,
                    linewidth=linewidth
                    )
                
                lines.append(line)

                # connection tos to nextnext neighbot

                # to right

                line = mlines.Line2D(
                    linecoords_x + vector[0],
                    linecoords_y + vector[1],
                    color=color_c_neighbors,
                    zorder=0,
                    linestyle='dashed',
                    linewidth=linewidth
                    )
                
                lines.append(line)

                # to left

                line = mlines.Line2D(
                    linecoords_x - 2 * vector[0],
                    linecoords_y - 2 * vector[1],
                    color=color_c_neighbors,
                    zorder=0,
                    linestyle='dashed',
                    linewidth=linewidth
                    )
                
                lines.append(line)


    # plotting lines

    for l in lines:
        ax.add_line(l)

    
    # options

    ax.set_aspect('equal')

    max_x, max_y = np.max(coords + vector, axis=0)[:2]
    min_x, min_y = np.min(coords - vector, axis=0)[:2]

    border_delta = constants.A_CC * .5

    ax.set_xlim(min_x - border_delta, max_x + border_delta)
    ax.set_ylim(min_y - border_delta, max_y + border_delta)

    ax.tick_params(
        which='both',
        direction='in'
        )
    
    ax.set_xlabel(r'$x$ [\AA]')
    ax.set_ylabel(r'$y$ [\AA]')

    fig.savefig(savedir + 'unit_cell.png', dpi=300, bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    main()