'''
class_network.py

Author: Tom Rodemund
Date: 20.07.2020

This class can constructs a network and fills the Hamiltonians accordingly.
'''

import time
import numpy as np
import copy
import random
import numpy.linalg as la

import matplotlib

from matplotlib import rc
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines

from modules.vertice_class import Vertice
from modules.atom_class import Atom
from modules.parameter_class import ParameterSet

import modules.network_methods as network_methods
import modules.quantum_transport as qt

from modules.constants import ETA
from modules.constants import CUTOFF
from modules.constants import A_CC

class Network:
    ####################################################################################
    # Basics
    ####################################################################################


    def __init__(self):
        '''
        Initialize

        All veriables listed here are necessary later on.
        '''
        # basics
        self.length = .0
        self.width = .0

        # vectors
        self.length_v = np.array([0.,0.,0.])
        self.width_v = np.array([0.,0.,0.])

        # dictionary with all vertices
        # format: vertice name - vertice
        self.vertice_list = []

        # lines of vertices the network consists of
        self.line_list = []

        # clusters, consisting of vertices
        self.cluster_list = []

        # list of vertices representing the two electrodes
        self.electrodes = []

        # lines to be decimated away with RDS and steps needed to do so
        self.RDA_lines = [[]]
        self.RDA_steps_needed = None

        # saving influence of electrodes
        self.gamma_list = []


    def read_input(self, inputdir='input/'):
        
        # reads unit cell
        inputname = inputdir + 'unit_cell.xyz'

        self.unit_cell = {}
        # n_atoms = np.loadtxt(inputname, skiprows=0, max_rows=1, dtype=int)
        # vector = np.loadtxt(inputname, skiprows=1, max_rows=1)

        f = open(inputname, 'r')
        n_atoms = int(f.readline())
        vector = [float(x) for x in f.readline().split('\t')]
        f.close()

        # save atoms
        coords = np.loadtxt(inputname, usecols=(1,2,3,), skiprows=2)
        atom_types = np.genfromtxt(inputname, usecols=(0,), skip_header=2, dtype=str)

        if n_atoms == 1:
            coords = [coords]
            atom_types = [atom_types]

        atom_list = []
        for c, t in zip(coords, atom_types):
            a = Atom(type=t, coords=c)
            atom_list.append(a)

        center = np.array([.0, .0, .0])

        self.unit_cell = Vertice(center, atom_list, vector)
        self.unit_cell.name = 'unit cell'
        
        # save hamiltonian and coupling matrix for one unit-cell to another
        qt.set_hamiltonian(self.DFTB_params, self.unit_cell)

        # set interaction to next cell in line
        v_temp = self.unit_cell.copy()
        v_temp.name = 'next'
        v_temp.center = v_temp.center + self.unit_cell.vector
        for a in v_temp.atoms:
            a.coords = a.coords + self.unit_cell.vector

        qt.set_coupling(self.DFTB_params, self.unit_cell, v_temp)


    def copy(self):
        '''
        Creates shallow copy of network based on the class dictionary.
        '''
        
        return copy.deepcopy(self)

    @property
    def n_vertices(self):
        '''
        Number of vertices as property
        '''
        return len(self.vertice_list)
    
    # print function

    def print(self):
        '''
        print all vertices with the vertice method
        '''
        print('THE NETWORK\n')

        # basics
        print('length = {0}'.format(self.length))
        print('width = {0}'.format(self.width))

        self.unit_cell.print()

        # lines of vertices the network consists of
        print('\nlines:')
        for line in self.line_list:
            print('\t- {0}'.format([v.name for v in line]))

        print('\nRDA-lines:')
        for line in self.RDA_lines:
            print('\t- {0}'.format([v.name for v in line]))

        # lines to be decimated away with RDS and steps needed to do so
        print('\nRDA steps needed = {0}'.format(self.RDA_steps_needed))

        # reads unit cell
        # print('unit-cell:')

        print('\n\nVertices ({0}):\n'.format(self.n_vertices))

        for key in sorted(self.vertice_list, key=lambda x: int(x)):
            v = self.vertice_list[key]
            v.print()
            print()

    # loading DFTB parameters

    def load_DFTB(self, target):
        '''
        Loads the parameterset in directory target for use.
        '''
        
        p = ParameterSet(target)

        self.DFTB_params = p


    ####################################################################################
    # Network Creation
    ####################################################################################

    def create(self, length, width, n_lines, n_linelength, layer_height):
        '''
        Creates a network consisting of cells defined in input/cell.xyz
        '''

        self.length = self.length_v[0] = length
        self.width = self.width_v[1] = width

        self.vertice_list = {}
        vertice_i = 0

        line_list = []

        theta_max = 2 * np.pi
        layer_vector = np.array([.0, .0, layer_height])
        cell_vector = self.unit_cell.vector

        # comparing cell-atoms to one another
        overlap_crit = A_CC * .5

        # generate the lines

        for i in range(n_lines):
            line = []

            # start point
            r0 = np.array([random.random() * self.length,  random.random() * self.width, .0])

            # angle
            theta = random.random() * theta_max
            
            # rotation matrix
            rotation_matrix = network_methods.rotation_matrix_xy(theta)

            # rotating step-vector
            dr = cell_vector @ rotation_matrix
            # rotating coordinates of unit cell
            cell_atoms = copy.deepcopy(self.unit_cell.atoms)
            for a in cell_atoms:
                a.coords = a.coords @ rotation_matrix


            # outer (dyadic) product multiplies each value of arange with the vector dr
            line_center_coords = np.outer(np.arange(n_linelength), dr) + r0

            for c in line_center_coords:
                vertice_i += 1
                vertice_name = '{0}'.format(vertice_i)

                atoms_new = []
                for a in cell_atoms:
                    coords_new = a.coords + c
                    atom_new = Atom(type=a.type, coords=coords_new)
                    atoms_new.append(atom_new)
                
                v = Vertice(c, atoms_new, dr)
                v.name = vertice_name

                self.vertice_list[vertice_name] = v

                line.append(v)
            


            # create sublines in case of line going over edge -> periodic boundary conditions
            shift_list = []

            for v in line:
                # add translation until vertice is inside network area
                x, y, z = v.center.copy()
                
                shift_v = np.array([0., 0., 0.])

                while True:
                    x, y, z = v.center + shift_v

                    if x < 0.:
                        shift_v = shift_v + self.length_v
                    elif self.length < x:
                        shift_v = shift_v - self.length_v
                    elif y < 0.:
                        shift_v = shift_v + self.width_v
                    elif self.width < y:
                        shift_v = shift_v - self.width_v
                    else:
                        break
                
                # add translation to coordinates
                v.center = v.center + shift_v
                for a in v.atoms:
                    a.coords = a.coords + shift_v
                
                # save shift_v to part in sublists
                shift_list.append(shift_v)



            # split into sublists
            sublines = []
            delta_max = 10**(-6)

            subline_now = [line[0]]
            shift_now = shift_list[0]

            for v, shift_v in zip(line[1:], shift_list[1:]):
                # same line
                if np.linalg.norm(shift_now - shift_v) < delta_max:
                    subline_now.append(v)
                # next line
                else:
                    sublines.append(subline_now)
                    subline_now = [v]
                    shift_now = shift_v
            
            sublines.append(subline_now)

            

            # Checks for overlaps. If that is the case, layer_vector is added and checking
            # starts all over again. All the if cases serve the purpose that all the loops
            # are ended once one overlap is found.

            for subline_now in sublines:
                while True:
                    is_overlapping = False

                    for line_ref in line_list:
                        if is_overlapping:
                            break

                        for v_1 in line_ref:
                            if is_overlapping:
                                break

                            for v_2 in subline_now:
                                if is_overlapping:
                                    break
                                
                                for a_1 in v_1.atoms:
                                    if is_overlapping:
                                        break

                                    c_1 = a_1.coords

                                    for a_2 in v_2.atoms:
                                        c_2 = a_2.coords
                                        dist = np.linalg.norm(c_1 - c_2)
                                        if dist < overlap_crit:
                                            is_overlapping = True
                                            break
                    
                    # if no overlaps are found, no action needs to be done and the while loop
                    # is escaped
                    if not is_overlapping:
                        break
                    # otherwise all atoms and cells are put in the next layer with the
                    # layer vector
                    else:
                        for v in subline_now:

                            v.center = v.center + layer_vector

                            for a in v.atoms:
                                a.coords = a.coords + layer_vector


                line_list.append(subline_now)
        
        # give correct line indizes
        n_lines_got = len(line_list)

        for line, i_line in zip(line_list, range(n_lines_got)):
            for v in line:
                v.line_index = i_line

        # save line_list
        self.line_list = line_list



    ####################################################################################
    # Network Manipulation
    ####################################################################################


    def initialize(self):
        '''
        Initializing the network

        - find neighbors
        '''
        
        # finding neighbors
        network_methods.connect_neighbors_line(self.line_list)


    def remove_overhang(self):
        '''
        Removing all vertices outside the network boundaries.
        '''

        delete_list = []

        for key in self.vertice_list:
            v = self.vertice_list[key]
            x, y = v.center[:2]

            if x < .0 or x > self.length or y < .0 or y > self.width:
                delete_list.append(key)
        
        for key in delete_list:
            v = self.vertice_list[key]
            self.vertice_list.pop(key)
            self.line_list[v.line_index].remove(v)
        
        


    def find_clusters(self):
        '''
        finding isolated clusters in the network, which are not interacting with one
        another
        '''

        # saves clusters consisting of vertices, lines are not seen here
        self.cluster_list = []
        
        # list of remaining vertices, as vertices already belonging to a cluster
        # need to be excluded from further search
        remaining_vertices = list(self.vertice_list.values())
        
        # finding all clusters
        while True:
            #print('\tfinding cluster...')
            cluster = []
            
            start_vertice = remaining_vertices[0]
            
            cluster.append(start_vertice)
            
            vertices_now = [start_vertice]
            vertices_next = []
            vertices_last = []
            
            # iterating over all vertices of cluster
            while True:
                
                for v in vertices_now:
                    neighbors_now = [self.vertice_list[i] for i in v.coupling.keys()]
                    
                    for n in neighbors_now:
                        
                        if (n not in vertices_last) and (n not in vertices_now) and (n not in vertices_next):
                            
                            vertices_next.append(n)
                
                # if no new vertices are found the cluster is finished
                if len(vertices_next) == 0:
                    break
                else:
                    cluster = cluster + vertices_next
                    
                    vertices_last = vertices_now
                    vertices_now = vertices_next
                    vertices_next = []
            
            self.cluster_list.append(cluster)
            
            # removing all vertices belonging to new cluster from list of considered vertices
            remaining_vertices = [v for v in remaining_vertices if v not in cluster]
            
            if len(remaining_vertices) == 0:
                break
    
    
    def only_percolating(self):
        '''
        Reduces the network to a single percolating cluster. If a percolating
        cluster is found returns True, otherwise returns False.
        
        If several clusters percolate, chooses the biggest one (the one with the 
        most vertices).
        
        If several vertices touch the electrodes, the first vertice encountered is 
        chosen. This could be subject to change later, if several contacts are
        used.
        '''

        # maximum distance to electrodes
        NEIGHBOR_CRIT = CUTOFF
        
        percolating_list = []
        
        # checks if cluster percolates for each cluster
        
        for cluster in self.cluster_list:
            
            is_touching_left = False
            is_touching_right = False
            
            # sorting by x-coordinates, so we only need to check leftmost and rightmost
            # vertice and have a deterministic way to get vertices connected to electrode
            cluster.sort(key=lambda v: v.center[0])

            vertice_left = cluster[0]
            vertice_right = cluster[-1]

            # checking for left side
            for a in vertice_left.atoms:
                x = a.coords[0]

                if x < NEIGHBOR_CRIT:
                    is_touching_left = True
                    break
            
            # checking for right side
            for a in vertice_right.atoms:
                x = a.coords[0]

                if self.length - x < NEIGHBOR_CRIT:
                    is_touching_right = True
                    break
            
            if is_touching_left and is_touching_right:
                percolating_list.append(cluster)
        

        # looking for largest percolating cluster
        
        if len(percolating_list) == 0:
            return False
        else:
            # getting cluster with the most vertices. arbitrary condition, but it's better
            # to have a deterministic one
            cluster_largest = max(self.cluster_list, key=len)

            
            # saving electrode positions

            self.electrodes = [cluster_largest[0], cluster_largest[-1]]
            cluster_largest[0].is_contractable = False
            cluster_largest[-1].is_contractable = False

            # updating network

            # only taking largest cluster
            self.cluster_list = [cluster_largest]
            
            # only taking vertices in that largest cluster
            self.vertice_list = {}
            for v in sorted(cluster_largest, key=lambda v: int(v.name)):
                self.vertice_list[v.name] = v
            
            # only taking lines part of largest cluster
            # only need to check one vertice, if one is part of cluster the
            # whole line is, as all vertices within one line are connected
            # to one another
            for i in range(len(self.line_list)):
                if not self.line_list[i][0] in cluster_largest:
                    self.line_list[i] = []

            return True


    def sep_RDA_lines(self):
        '''
        ATTENTION: this function isn't ised right now and only left in for
        potential future use.

        Gets lines with N = 2^n + 1 entries, where each element has only 2 neighbors. Only the ends may
        more neighbors, as they aren't decimated away with RDS.

        This method drastically reduces the number of decimation steps necessary for a sparse network,
        as we use the fact that all lines can be treated similarly.

        First, all lines consisting of 3+ vertices are found, then sliced into lines with 2^n + 1 entries
        for RDS.
        '''

        # finding all lines with atoms of 2 neighbors

        all_lines = self.line_list

        RDS_line_list_start = []

        for line in all_lines:
            n_vertices = len(line)

            sublines = []

            subline_now = []

            # i starts at 1, as beginning of line can only be corner of line
            # and will be added retroactively as corner if vertice i only has
            # 2 neighbors
            for i in range(1, n_vertices):
                v = line[i]
                n_neighbors = v.n_neighbors

                # vertice in line found
                if n_neighbors == 2:
                    # if line is empty, vertice i-1 needs to be added as starting point
                    if len(subline_now) == 0:
                        subline_now.append(line[i-1])
                    
                    subline_now.append(v)
                # last entry contains vertice with more or less than two neighbors
                elif n_neighbors != 2 and len(subline_now) > 0:
                    subline_now.append(v)

                    sublines.append(subline_now)

                    subline_now = []
            
            RDS_line_list_start += sublines

        # now deviding them into sublines with 2^k + 1 entries

        RDS_line_list_end = []

        # steps needed is equal tu highest k found
        RDA_steps_needed = 0

        for line in RDS_line_list_start:
            # starting from the beginning
            index_start = 0

            # while loop will be escaped once no sublines are left
            while True:
                # N = 2^k + 1 <=> k = log(N-1) / log(2) = log2(N-2)
                # N & n are integers, so we take n_max with N < N_max

                # maximum entries of this subchain
                N_max = len(line[index_start:])
                
                if N_max < 2:
                    break

                # calculate k as above
                k_now = int( np.floor( np.log2(N_max-1) ) )

                if k_now > RDA_steps_needed:
                    RDA_steps_needed = k_now

                # no of entries of this subchain
                N_now = 2**k_now + 1

                # escaping the while loop if found chain is too short
                if N_now < 3:
                    break
                # otherwise append it to sublinechain
                else:
                    index_end = index_start + N_now

                    subline = line[index_start:index_end]
                    RDS_line_list_end.append(subline)

                    # 1 lower as index_end, as endpoints can be used by either subchain
                    index_start = index_end - 1

        # marking all vertices as to be decimated via RDA

        for line in RDS_line_list_end:
            for v in line[1:-1]:
                v.is_RDA_contractable = True

        # saving
 
        self.RDA_lines = RDS_line_list_end
        self.RDA_steps_needed = RDA_steps_needed


    ####################################################################################
    # Quantum Transport Stuff
    ####################################################################################

    def set_coupling(self):
        '''
        Sets coupling and hamiltonians for all vertices not in a RDA-line. Resets everything.
        '''

        for v in self.vertice_list.values():
            # check if vertice is in an RDA-line
            # if so, no coupling will be determined
            qt.set_hamiltonian(self.DFTB_params, v)

            for neighbor_name in v.coupling:
                neighbor = self.vertice_list[neighbor_name]
                if v.coupling[neighbor_name] is None:
                    qt.set_coupling(self.DFTB_params, v, neighbor)

    
    def matrix_corrections(self, E):
        '''
        Applies corrections to matrices to transform them to an orthogonal basis.

        H = H_0 + ( 1 - S ) * E
        tau = tau_0 - sigma * E
        '''

        # correcting vertices

        for v in self.vertice_list.values():
            # correcting H
            H = v.hamiltonian
            S = v.overlap

            v.hamiltonian = H + ( np.identity(H.shape[0]) - S ) * E
            v.overlap = None # set to none to check if vertice has been modified

            # correcting coupling
            for neighbor_name in v.coupling:
                tau = v.coupling[neighbor_name]
                sigma = v.sigmas[neighbor_name]

                v.coupling[neighbor_name] = tau - sigma * E
                v.sigmas[neighbor_name] = None


    def perform_RDA(self, E):
        '''
        Performs RDA, calculating Green's function of the elctrodes and coupling between
        borders of RDA-chains simultaneously.
        '''

        RDA_max_steps = 100
        convergence_crit = 10**(-3)

        gamma_list_now = []

        # perform RDA for each electrode individually
        # iterating over i to distinguish between left and right
        for i in range(2):
            v = self.electrodes[i]
            vector = v.vector

            # preparing electrode vertices

            v_L = Vertice( # center vertice
                center=np.array([.0,.0,.0]),
                atoms=v.atoms,
                vector=vector
                )
            v_L.name = 'L'
            
            v_R = v_L.copy()
            v_R.name = 'R'
            for atom in v_R.atoms:
                atom.coords = atom.coords + vector
            
            # getting parameters
            qt.set_coupling(self.DFTB_params, v_L, v_R)
            t_LR = v_L.coupling['R'] - E * v_L.sigmas['R']
            t_RL = v_R.coupling['L'] - E * v_R.sigmas['L']

            # parameters for RDA
            ID = np.identity(v.hamiltonian.shape[0])
            H_B = H_L = H_R = v.hamiltonian# hamiltionian is already corrected

            alpha = t_LR
            beta = t_RL

            for j in range(RDA_max_steps):
                G_B = la.inv( (E + ETA) * ID - H_B )

                alpha_new = alpha @ G_B @ alpha
                beta_new = beta @ G_B @ beta

                interaction_right = alpha @ G_B @ beta
                interaction_left = beta @ G_B @ alpha

                H_B_new = H_B + interaction_left + interaction_right
                H_L_new = H_L + interaction_right
                H_R_new = H_R + interaction_left
                
                # cycling
                alpha = alpha_new
                beta = beta_new
                H_B = H_B_new
                H_L = H_L_new
                H_R = H_R_new

                # check if finished
                norm = la.norm(alpha) + la.norm(beta)
                if norm < convergence_crit:
                    break
            
            if j == RDA_max_steps-1:
                print('WARNING: RDA not converged')

            # adding interaction to hamiltonian

            # add interaction from left
            if (vector[0] > 0. and i == 0) or (vector[0] < 0. and i == 1):
                G_R = la.inv( (E + ETA) * ID - H_R )
                sigma = t_RL @ G_R @ t_LR
            # add interaction from right
            else:
                G_L = la.inv( (E + ETA) * ID - H_L )
                sigma = t_LR @ G_L @ t_RL

            # interaction added
            v.hamiltonian = v.hamiltonian + sigma

            gamma = 1j * (sigma - sigma.conjugate().T)
            gamma_list_now.append(gamma)
    
        self.gamma_list = gamma_list_now


            

    def contract_single(self, E):
        '''
        Contracts the vertice with the least neighbors.
        '''
        # find vertice with the least neighbors

        all_vertices = sorted(self.vertice_list.values(), key=lambda v: v.n_neighbors)


        # takes the first contractable vertice it finds
        # breaks immediatly after, as only one vertice is to be decimated
        for v in all_vertices:
            if v.is_contractable:
                v_0 = v
                break
        
        # starting contraction process

        '''
        Contracts the vertice via RDS. Modifies all neighbor vertices and
        rewires them, so they connect to one another.

        code:
        - 0... vertice to be contracted
        - 1... neighbor number 1
        - 2... neighbor number 2
        '''


        ID = np.identity(v_0.hamiltonian.shape[0])
        neighbors = [self.vertice_list[key] for key in v_0.coupling.keys()]

        G_0 = np.linalg.inv( (E + ETA) * ID - v_0.hamiltonian )

        for v_n_1 in neighbors:
            tau_01 = v_0.coupling[v_n_1.name]
            tau_10 = v_n_1.coupling[v_0.name]

            # modify H of neighbors
            H_delta = tau_10 @ G_0 @ tau_01
            v_n_1.hamiltonian = v_n_1.hamiltonian + H_delta

            # rewiring

            # remove connection to decimated vertice
            v_n_1.coupling.pop(v_0.name)

            for v_n_2 in neighbors:
                if v_n_1 is not v_n_2:
                    # calculating new interaction
                    tau_02 = v_0.coupling[v_n_2.name]
                    tau_12 = tau_10 @ G_0 @ tau_02

                    # if n_1 is already connected to n_2, new coupling is
                    # simply added
                    if v_n_2.name in v_n_1.coupling.keys():
                        v_n_1.coupling[v_n_2.name] = v_n_1.coupling[v_n_2.name] + tau_12
                    # otherwise a new interaction appears
                    else:
                        v_n_1.coupling[v_n_2.name] = tau_12
        
    
        # deleting vertice
        self.vertice_list.pop(v_0.name)
        self.cluster_list[0].remove(v_0)
        self.line_list[v.line_index].remove(v_0)


    def perform_FIS(self, E):
        '''
        If only two vertices are remaining, connected to the electrodes, FIS
        is used to calculate the device's Green's function and also the
        transmission. Returns the transmission.s
        '''

        # checking if only two vertices remain
        if self.n_vertices != 2:
            print('ERROR: FIS cannot be performed, as {0} vertices remain!'.format(self.n_vertices))

        v_L, v_R = self.electrodes
        
        # left Green's function
        H_1 = v_L.hamiltonian # already includes sigma_L
        ID_1 = np.identity( H_1.shape[0] )
        G_1 = np.linalg.inv( (E+ETA) * ID_1 - H_1 )
        
        # correction to right hamiltonian from left hamiltonian
        H_delta = v_R.coupling[v_L.name] @ G_1 @ v_L.coupling[v_R.name]

        # right Green's function
        H_2 = v_R.hamiltonian + H_delta # already includes sigma_R
        ID_2 = np.identity( H_2.shape[0] )
        G_2 = np.linalg.inv( (E+ETA) * ID_2 - H_2 )

        # finalizing
        G_ges = G_2 @ v_R.coupling[v_L.name] @ G_1
        
        gamma_L, gamma_R = self.gamma_list

        M = gamma_R @ G_ges @ gamma_L @ G_ges.conjugate().T
        T = np.trace(M)

        return T.real


    
    
    
    ####################################################################################
    # Graphics
    ####################################################################################
    
    
    def plot_vertices(self, mode='layers', title=None, plot_names=False):
        '''
        Plotting the network in the specified mode. The idea is that we generate a list
        containing the color of each vertice.

        Modes:
        
        '''

        fig, ax = plt.subplots()

        patches = []
        lines = []

        linecolor = 'black'

        # radius of plotted atoms
        R = A_CC * .5

        all_vertices = list(self.vertice_list.values())
        facecolor_list = []

        if mode == 'layers':
            '''
            coloring in the vertices according to z-coordinate
            '''
            # mode to be displayed as plot_title
            displaymode = mode

            # get min and max z-coordinate
            z_min = z_max = all_vertices[0].center[2]

            z_coordinates = []

            for v in all_vertices:
                z = v.center[2]

                # not sure if this works properly every time
                if z not in z_coordinates:
                    z_coordinates.append(z)

                if z < z_min:
                    z_min = z
                elif z > z_max:
                    z_max = z
            
            z_delta = z_max - z_min
            
            # specifying colors

            colormap = plt.get_cmap('brg')

            for v in all_vertices:
                z = v.center[2]

                colorindex = (z - z_min) / z_delta
                facecolor = colormap(colorindex)

                facecolor_list.append(facecolor)
        
        elif mode == 'clusters':
            '''
            coloring in the vertices according to cluster
            '''
            # mode to be displayed as plot_title
            displaymode = mode

            n_clusters = len(self.cluster_list)

            cluster_colors = ['C{0}'.format(i%10) for i in range(n_clusters)]

            # iterating over all clusters to see which one the vertice belongs to
            for v in all_vertices:
                for i in range(n_clusters):
                    cluster_now = self.cluster_list[i]

                    if v in cluster_now:
                        facecolor = cluster_colors[i]
                        facecolor_list.append(facecolor)
                        break
            
        elif mode == 'RDA_lines':
            '''
            coloring in the vertices according to line
            
            line middle parts have color, edges are white
            '''
            # mode to be displayed as plot_title
            displaymode = 'RDA lines'

            # initializing facecolor_list, as not all vertices are in a RDA_line
            facecolor_list = ['black' for v in all_vertices]
            
            n_lines = len(self.RDA_lines)
            
            line_colors = ['C{0}'.format(i%10) for i in range(n_lines)]

            
            for i in range(len(all_vertices)):
                v = all_vertices[i]

                for j in range(n_lines):
                    line_now = self.RDA_lines[j]

                    if v in line_now:
                        if v == line_now[0] or v == line_now[-1]:
                            facecolor = 'white'
                        else:
                            facecolor = line_colors[j]

                        facecolor_list[i] = facecolor
                        break
            

        else:
            print('ERROR: mode {0} not recognized'.format(mode))
            return
        
        # creating patches

        for v, facecolor in zip(all_vertices, facecolor_list):
            # painting circles

            x, y = v.center[:2]

            circle = mpatches.Circle(
                (x, y),
                radius=R,
                facecolor=facecolor,
                edgecolor=linecolor,
                )
            
            patches.append(circle)

            # painting lines

            for name in v.coupling.keys():
                neighbor = self.vertice_list[name]
                x_n, y_n = neighbor.center[:2]

                x_list = [x, x_n]
                y_list = [y, y_n]

                line = mlines.Line2D(
                    x_list,
                    y_list,
                    color=linecolor,
                    zorder=0,
                    linestyle='-'
                    )
                
                lines.append(line)

        # draw electrodes
        n_electrodecells = 5

        for i in range(len(self.electrodes)):
            v = self.electrodes[i]
            vector = v.vector
            coords_start = v.center

            if (i == 0 and vector[0] > 0.) or (i == 1 and vector[0] < .0):
                vector = vector * (-1.)

            
            # drawing

            for i in range(n_electrodecells):
                coords_now = (i+1) * vector + coords_start
                circle = mpatches.Circle(
                    (coords_now[0], coords_now[1]),
                    radius=R,
                    facecolor='black',
                    edgecolor=linecolor,
                    )
                patches.append(circle)
            
            line = mlines.Line2D(
                [coords_start[0], coords_start[0] + n_electrodecells * vector[0]],
                [coords_start[1], coords_start[1] + n_electrodecells * vector[1]],
                color=linecolor,
                zorder=0,
                linestyle='-'
                )
            lines.append(line)

        # draw box containing the network underneath the vertices

        rectangle = mpatches.Rectangle(
            (.0, .0),
            self.length,
            self.width,
            facecolor='lightgray',
            zorder=-1
            )

        patches.insert(0, rectangle)

        # adding all patches and lines

        for l in lines:
            ax.add_line(l)

        for p in patches:
            ax.add_patch(p)

        # adding names of vertices if needed

        if plot_names:
            for v in all_vertices:
                x, y = v.center[:2]
                name = v.name

                ax.text(x, y, name, ha='center', va='center', fontsize=2, color='white')

        # options for plot_layout

        ax.set_aspect('equal')
        ax.set_axis_off()

        ax.tick_params(
            which='both',
            direction='in'
            )

        delta_borders = 5.

        ax.set_xlim(.0 - delta_borders, self.length + delta_borders)
        ax.set_ylim(.0 - delta_borders, self.width + delta_borders)

        ax.set_xlabel(r'$x$ [\AA]')
        ax.set_ylabel(r'$y$ [\AA]')

        # ax.set_title(displaymode)

        if title is None:
            plot_title = 'network_{0}.png'
        else:
            plot_title = title

        fig.savefig(plot_title, bbox_inches='tight', dpi=300)
        
        plt.close()




    def plot_atoms(self, title=None):
        '''
        plotting the atoms
        '''
        R_C = A_CC * .3
        R_H = A_CC * .1

        patches = []
        lines = []
    
        fig, ax = plt.subplots()

        all_vertices = list(self.vertice_list.values())

        all_atoms = []

        for v in all_vertices:
            for a in v.atoms:
                all_atoms.append(a)
        
        all_atoms.sort(key=lambda a: a.coords[2])


        # for colormap
        z_min = all_atoms[0].coords[2]
        z_max = all_atoms[-1].coords[2]
        z_delta = z_max - z_min

        colormap = plt.get_cmap('brg')

        for a in all_atoms:
            x, y, z = a.coords
            colorindex = ( z - z_min ) / z_delta
            facecolor = colormap(colorindex)

            if a.type == 'C':
                radius = R_C
            elif a.type == 'H':
                radius = R_H
            else:
                print('ERROR: type {0} not recognized'.format(a.type))
                radius = .0

            circle = mpatches.Circle(
                (x, y),
                radius=radius,
                facecolor=facecolor
                )
            
            patches.append(circle)


        # draw box containing the network underneath the vertices

        rectangle = mpatches.Rectangle(
            (.0, .0),
            self.length,
            self.width,
            facecolor='lightgray',
            zorder=0
        )

        patches.insert(0, rectangle)

        # adding all patches and lines

        for l in lines:
            ax.add_line(l)

        for p in patches:
            ax.add_patch(p)

        # options for plot_layout

        ax.set_aspect('equal')
        ax.set_axis_off()

        ax.tick_params(
            which='both',
            direction='in'
            )

        delta_borders = 5

        ax.set_xlim(.0 - delta_borders, self.length + delta_borders)
        ax.set_ylim(.0 - delta_borders, self.width + delta_borders)

        if title is None:
            plot_title = 'network_{0}.png'
        else:
            plot_title = title

        fig.savefig(plot_title, bbox_inches='tight', dpi=300)
        
        plt.close()

        


    def save_xyz(self, title='network.xyz'):
        '''
        Saves the network in .xyz format to be plotted in 3D with M;athematica or the likes
        '''
        atom_list = []

        for line in self.line_list:
            for v in line:
                for a in v.atoms:
                    atom_line = [a.type, *a.coords]
                    atom_list.append(atom_line)
        
        n_entries = len(atom_list)

        f = open('results/{0}'.format(title), 'w')

        f.write('{0}\nnetwork atoms\n'.format(n_entries))

        for line in atom_list:
            f.write('{0}\t{1}\t{2}\t{3}\n'.format(*line))
        
        f.close()


    def save_neighborhoods(self, title='neighborhood.txt'):
        
        f = open(title, 'w')
        
        for verticename, vertice in self.vertice_list.items():
            outstring = verticename
            
            for neighborname in vertice.coupling.keys():
                outstring += ',{0}'.format(neighborname)
            
            outstring += '\n'
            
            f.write(outstring)
                
        f.close()
    
    
    
    
    