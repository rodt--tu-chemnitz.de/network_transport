'''
Author: Tom Rodemund
Date 02.09.2020

Constants needed to be the same in all methods during calculation
'''

# imaginary value for calculation of Green's function
ETA = 10**(-5) * 1j

# carbon-carbon distance
A_CC = 1.42 # Anstrom

# carbon-hydrogen distance
A_CH = 1.08

# interaction cutoff-radius
# 3NN interaction
CUTOFF = A_CC * 2.6