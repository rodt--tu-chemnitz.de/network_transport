'''
Author: Tom Rodemund
Date: 21.08.2020

This module contains the necessary functions for to calculate the transmission
spectrum and the conductivity via the quantum-transport formalism.
'''

import numpy as np

import scipy.constants as sc
e = sc.elementary_charge
k = sc.Boltzmann / e

from modules.constants import A_CC
from modules.constants import CUTOFF


#################################################################
# setting the coupling matrices
#################################################################

ONSITE_ENERGY = .0
def set_hamiltonian(p, v):
    '''
    constructing the hamiltonian and overlap matrix
    of this vertice
    '''

    H_total = []
    S_total = []

    for a_1 in v.atoms:
        H_line = []
        S_line = []

        for a_2 in v.atoms:
            # checking if onsite energies are needed or not
            if a_1 == a_2:
                H, S = p.get_onsite_energies(a_1)
            else:
                H, S = p.get_interaction(a_1, a_2)
                            
            H_line.append(H)
            S_line.append(S)
        
        H_total.append(H_line)
        S_total.append(S_line)
    
    # fusing submatrizes to big ones
    H_total = np.block(H_total)
    S_total = np.block(S_total)

    # saving
    v.hamiltonian = H_total
    v.overlap = S_total


def set_coupling(p, v_1, v_2):
    '''
    Calculate and sets interaction for two vertices v_1 and v_2. Coupling is
    calculated between each combionation of atoms.
    '''

    tau_total = []
    sigma_total = []

    for a_1 in v_1.atoms:
        tau_line = []
        sigma_line = []

        for a_2 in v_2.atoms:
            tau, sigma = p.get_interaction(a_1, a_2)
                            
            tau_line.append(tau)
            sigma_line.append(sigma)
        
        tau_total.append(tau_line)
        sigma_total.append(sigma_line)
    
    tau_total = np.block(tau_total)
    sigma_total = np.block(sigma_total)
    
    # saving it

    v_1.coupling[v_2.name] = tau_total
    v_1.sigmas[v_2.name] = sigma_total
    # dagger normally, but all values are real numbers here
    v_2.coupling[v_1.name] = tau_total.T
    v_2.sigmas[v_1.name] = sigma_total.T



def rotated_matrix(M, theta, atoms_1, atoms_2=None):
    '''
    Returns the Matrix M rotated by theta rad.

    atoms_1 and atoms_2 are the atom lists of the necessary cells,
    which are needed to get the right dimensions of the submatrices
    '''

    # if hamiltonian is calculated
    if atoms_2 == None:
        atoms_2 = atoms_1

    cos = np.cos(theta)
    sin = np.sin(theta)

    M_lines = []

    i0 = 0

    for a_1 in atoms_1:
        line = []

        # number of orbitals
        if a_1.type == 'C':
            di = 4
        elif a_1.type == 'H':
            di = 1
        else:
            print('ERROR: atom type {0} not implemented'.format(a_1.type))


        j0 = 0

        for a_2 in atoms_2:
            # diagonal elements (onsite energies)
            # stay the same
            if a_1 == a_2:
                dj = di
            # non-diagonal elements
            elif a_2.type == 'C':
                dj = 4
            elif a_2.type == 'H':
                dj = 1
            else:
                print('ERROR: atom type {0} not implemented'.format(a_1.type))
            
            submatrix = M[i0:i0+di,j0:j0+dj]

            '''
            Now this gets kind of long, but I don't now a more efficient way
            to write this right now.

            The double for-loop lets us avoid if-else clauses to distinguish
            between C-C, C-H, H-C, H-H bonds.
            '''

            # reading all matrix values

            s_s = submatrix[0,0]

            len_1, len_2 = submatrix.shape

            # C-H and C-C
            if len_1 == 4:
                px_s = submatrix[1,0]
                py_s = submatrix[2,0]
                pz_s = submatrix[3,0]
            
            # H-C and C-C
            if len_2 == 4:
                s_px = submatrix[0,1]
                s_py = submatrix[0,2]
                s_pz = submatrix[0,3]
            
            # C-C
            if len_1 == 4 and len_2 == 4:
                px_px = submatrix[1,1]
                px_py = submatrix[1,2]
                px_pz = submatrix[1,3]

                py_px = submatrix[2,1]
                py_py = submatrix[2,2]
                py_pz = submatrix[2,3]

                pz_px = submatrix[3,1]
                pz_py = submatrix[3,2]
                pz_pz = submatrix[3,3]

            # saving transformed matrix
            
            submatrix_rot = np.zeros(submatrix.shape, dtype=complex)

            # checking if matrix is zero via frobenius-norm
            if np.linalg.norm(submatrix) > .001:
                # always needed
                submatrix_rot[0,0] = s_s

                if len_1 == 4:
                    # <px|s>
                    submatrix_rot[1,0] = px_s * cos - py_s * sin
                    # <py|s>
                    submatrix_rot[2,0] = px_s * sin + py_s * cos
                    # <pz|s>
                    submatrix_rot[3,0] = pz_s
                
                if len_2 == 4:
                    # <s|px>
                    submatrix_rot[0,1] = s_px * cos - s_py * sin
                    # <s|py>
                    submatrix_rot[0,2] = s_px * sin + s_py * cos
                    # <s|pz>
                    submatrix_rot[0,3] = s_pz
                
                if len_1 == 4 and len_2 == 4:
                    # <px|px>
                    submatrix_rot[1,1] = px_px * cos**2 + py_py * sin**2 - sin * cos * (px_py + py_px)
                    # <px|py>
                    submatrix_rot[1,2] = (px_px - py_py) * sin * cos + px_py * cos**2 - py_px * sin**2
                    # <px|pz>
                    submatrix_rot[1,3] = px_pz * cos - py_pz * sin

                    # <py|px> (caution: NEARLY identical to <px|py>)
                    submatrix_rot[2,1] = (px_px - py_py) * sin * cos + py_px * cos**2 - px_py * sin**2
                    # <py|py>
                    submatrix_rot[2,2] = px_px * sin**2 + py_py * cos**2 + sin * cos * (px_py + py_px)
                    # <py|pz>
                    submatrix_rot[2,3] = px_pz * sin + py_pz * cos

                    # <pz|px>
                    submatrix_rot[3,1] = pz_px * cos - pz_py * sin
                    # <pz|py>
                    submatrix_rot[3,2] = pz_px * sin + pz_py * cos
                    # <pz|pz>
                    submatrix_rot[3,3] = pz_pz


            # print(a_1.type, '\t', a_2.type)
            # print_matrix(submatrix)


            j0 += dj
            line.append(submatrix_rot)
        

        i0 += di
        M_lines.append(line)
    
    return np.block(M_lines)





#################################################################
# conductivity
#################################################################

def calc_conductivity(E_list, T_list, temperature):
    '''
    Calculate the conductivity via the Landauer-Büttiker formalism using the
    trapezoid rule. E_F = 0 is assumed, so the E_list needs to be rescaled in 
    that way.
    '''
    n_datapoints = len(E_list)

    if n_datapoints != len(T_list):
        print('ERROR: conductivity can not be calculated, E_list and T_list have different lengths ({0}, {1})'.format(len(E_list), len(T_list)))
        return .0

    kT = k * temperature

    y_list = []
    for E, T in zip(E_list, T_list):
        y = T / ( 2 * kT * ( 1 + np.cosh(E/kT) ) )
        y_list.append(y)

    integral_list = []
    for i in range(n_datapoints-1):
        subsum = (E_list[i+1]-E_list[i]) * (y_list[i+1]+y_list[i]) * .5
        integral_list.append(subsum)
    
    return np.sum(integral_list)


#################################################################
# MISC
#################################################################

def print_matrix(M):
    for line in M:
        for x in line:
            print('{0:.2f}'.format(x), end='\t')
        print()

def get_angle_to_x(v):
    '''
    gives the angle between the vector v and the x axis
    '''
    v_x = [1., 0., 0.]

    # norming
    vector = np.array(v) / np.linalg.norm(v)

    theta = np.arccos( np.dot(v_x, vector) )

    if v[1] < 0:
        theta = - theta
    
    return theta