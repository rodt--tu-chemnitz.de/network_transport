import os

import modules.bandstructure as bs
import modules.plotting as plotting

from modules.parameter_class import ParameterSet

def main(create_plots=True):
    '''
    calculating band structure and fermi energy of specified unit_cell
    '''

    print('Analyzing unit cell')

    celldir = 'unit_cell/'
    savedir = celldir
    DFTBdir = 'DFTB_CHNO/Hamilton/'

    print('\t- reading DFTB parameters')

    p = ParameterSet(DFTBdir)

    print('\t- calculating band structure')

    bandstructure = bs.calculate_bandstructure(p, cellpath=celldir, savedir=savedir)

    print('\t- calculating fermi-energy')

    fermienergy = bs.calculate_fermienergy(bandstructure, savedir=savedir)
    
    # fermienergy = -4.74068862094

    if create_plots == True:
        print('\t- plotting')
        plotting.bandstructure(bandstructure, savedir=savedir, fermienergy=fermienergy)
        plotting.unit_cell(celldir=celldir, savedir=celldir)
    

if __name__ == '__main__':
    # targetdir = 'cell_line_2atoms/unit_cell/'
    # plotting.unit_cell(celldir=targetdir, savedir=targetdir)
    main()