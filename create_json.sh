# arguments
length=50
width=20
n_lines=20
n_linelength=20.0
layer_height=1.42
seed=-1
is_plotting=false
fermi_energy=-5.755293457086019249e-02
temperature=300.0
inputdir="./"
outputdir="results/"

# joining them together
args="$length $width $n_lines $linelength $layer_height $seed $is_plotting $temperature $inputdir $outputdir"

# executing program
module load python3
python3 create_json.py $args