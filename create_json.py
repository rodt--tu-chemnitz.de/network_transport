'''
Author: Tom Rodemund
Date: 08.09.2020

This file creates the input json file for the main programm
and accepts arguments via the command zone.
'''

import json
import sys
import time

def main():
    '''
    sys.argv arguments:

    0... script name
    1... network length
    2... network width
    3... n_lines
    4... linelength
    5... layer_height
    6... seed
    7... is_plotting
    8... temperature
    9... inputdir
    10... outputdir
    11... DFTBdir
    '''
    
    # reading the command line arguments

    parameters = {}

    parameters['length'] = float(sys.argv[1])
    
    parameters['width'] = float(sys.argv[2])

    parameters['n_lines'] = int(sys.argv[3])

    parameters['linelength'] = float(sys.argv[4])

    parameters['layer_height'] = float(sys.argv[5])

    parameters['seed'] = float(sys.argv[6])
    if parameters['seed'] < 0:
        parameters['seed'] = time.time()

    parameters['is_plotting'] = bool(int(sys.argv[7]))

    parameters['temperature'] = float(sys.argv[8])

    parameters['inputdir'] = str(sys.argv[9])

    parameters['outputdir'] = str(sys.argv[10])

    parameters['DFTBdir'] = str(sys.argv[11])


    # saving as json
    
    with open('input.json', 'w') as outfile:
        json.dump(parameters, outfile)


if __name__ == '__main__':
    main()