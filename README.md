# network_transport

## Goal

Calculating the transmission and conductance of a carbon-based network using the quantum transport formalism and the Landauer-Büttiker formalism. This code is part of my (Tom Rodemund) master's thesis.

These scripts are to be calculated on the MAINSIM at TU-Chemnitz.

## Progress

Implemented...

- [x] network generation
- [x] quantum-transport formalism to calculate transmission
- [x] landauer-formalism to calculate conductivity
- [x] DFTB-parameters for hamiltonian construction
- [x] analyze network properties

***Transmission spectra can now be calculated for any network consisting of carbon and hydrogen atoms!***


## Parameter sets for percolating test systems

### Set 1: large, percolating network

```
{
    "length": 50,
    "width" : 20,
    "n_lines" : 20,
    "linelength" : 15,
    "layer_height" : 1.42,
    "seed" : 1,
    "is_plotting" : true,
    "temperature" : 300.0,
    "inputdir" : "unit_cell/",
    "outputdir" : "results/",
    "DFTBdir" : "DFTB_CHNO/Hamilton/"
}
```

### Set 2: linear chain to test algorithm

```
{
    "length": 10,
    "width" : 5,
    "n_lines" : 1,
    "linelength" : 20,
    "layer_height" : 1.42,
    "seed" : 6,
    "is_plotting" : true,
    "temperature" : 300.0,
    "inputdir" : "unit_cell/",
    "outputdir" : "results/",
    "DFTBdir" : "DFTB_CHNO/Hamilton/"
}
```

### Set 3: two chains overlapping

```
{
    "length": 20,
    "width" : 15,
    "n_lines" : 2,
    "linelength" : 13,
    "layer_height" : 3.35,
    "seed" : 11,
    "is_plotting" : true,
    "temperature" : 300.0,
    "inputdir" : "unit_cell/",
    "outputdir" : "results/",
    "DFTBdir" : "DFTB_CHNO/Hamilton/"
}
```


## Remarks

### Speed

The code scales linearly with the number of unit cells und cubic with the size/number of atoms of a unit cell.

Initially, a method of "fast decimation" was to be used for long, unperturbed chains having the same unit cells. The idead was, that RDA (renormalization decimation algorithm) needed to be used to calculate the self-energies and broadening matrices of the electrodes anyways. The corrections to the bulk hamiltonians and their coupling matrices calculated in the process could also be applied to the aforementioned linear chains, as they are similar to the bulk (besides not being infinitely long).

However, the hamiltonians are constructed of orbitals in a fixed coordinate system (s, px, py, pz). Thus, the hamiltonians and their corrections depend on the rotation of the unit cell. It should be possible to calculate these values once and apply them to all vertices through a basis transformation of the regarding matrices, but that could not be achieved. I settled for a more general (but slower) approach to perform RDA for each electrode individually and treat RDA-contractable vertices normally by contracting them one by one.

The speed gain of "fast decimation" would be nice, but it is unclear how big of an impact it would have. For large unit cells, the matrix inversion is the numerically costly process and this method would skip some of those, but the benefit lies in the linearly scaling realm. The cubic scaling of the unit cell size perhaps dominates that influence. 

A simpler way to apply "fast decimation" to the network could also be to perform RDA for each linear chain seperately, significantly reducing the number of necessary matrix inverions for long chains.

### Future additions

The quantum-transport can be used to treat a huge variety of problems. Right now, the networks consists only of perfect unit cells. You could also mix things up by adding defect cells, which perturb the electron transport even further.

Carbon-based networks could be used in transistors between two long electrodes. For now, the network is only contacted on two points: one on the left, one on the right. However, an arbitrary number of contacts on each side of the network is possible to reflect the network being contacted by the aformentioned long electrodes. With the landauer-formalism, the conductance between these can be obtained by calculating the transmission between each combination of left- and right-side contacts.